import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { BusinessModel } from '../models/business.model';
import { Connection } from '../models/connection';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class BusinessService {

  private connection: Connection = new Connection;

  constructor(private http: HttpClient) { }

  public getBusiness(): Observable<BusinessModel[]> {
    return this.http.get<BusinessModel[]>(this.connection.URL + 'business');
  }

  public addBusiness(business: BusinessModel): Observable<BusinessModel> {
    
    return this.http.post<BusinessModel>(this.connection.URL + 'business/create', 
    JSON.stringify(business), 
    httpOptions);
  }

}

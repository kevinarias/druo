import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateBusinessComponent }  from './components/create-business/create-business.component';
import { ViewBusinessComponent }  from './components/view-business/view-business.component';

const routes: Routes = [
  { path: '', redirectTo: 'negocios', pathMatch: 'full' },
  { path: 'crear', redirectTo: 'negocios/crear', pathMatch: 'full' },
  { path: 'negocios', component: ViewBusinessComponent },
  { path: 'negocios/crear', component: CreateBusinessComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

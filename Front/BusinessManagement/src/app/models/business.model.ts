export class BusinessModel {
    id: number;
    name: string;
    nit: number;
    mail: string;
}
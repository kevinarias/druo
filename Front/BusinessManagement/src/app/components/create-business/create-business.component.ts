import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';

import { BusinessModel } from '../../models/business.model'
import { BusinessService } from '../../services/business.service';

@Component({
  selector: 'app-create-business',
  templateUrl: './create-business.component.html',
  styleUrls: ['./create-business.component.scss'],
  providers: [BusinessService]
})
export class CreateBusinessComponent implements OnInit {

  public business: BusinessModel = new BusinessModel;

  constructor(private BusinessService: BusinessService, private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  save() {
    let validate = this.validate(this.business);
    if (validate == "SUCCESS") {
      this.BusinessService.addBusiness(this.business).subscribe(
        res => {
          let msg = "El negocio <" + this.business.name + "> fue creado con éxito.";
          this.snackBar.open(msg, 'Cerrar', { duration: 4000, panelClass: ["snackbar-success"] });
        },
        error => {
          if (error.status == 400) {
            let err = "Error en la petición."
            this.snackBar.open(err, 'Cerrar', { duration: 3000, panelClass: ["snackbar-error"] });
          } else if (error.status == 412) {
            let err = "Error en el campo del negocio."
            this.snackBar.open(err, 'Cerrar', { duration: 3000, panelClass: ["snackbar-error"] });
          } else {
            let err = "Error desconocido."
            this.snackBar.open(err, 'Cerrar', { duration: 3000, panelClass: ["snackbar-error"] });
          }
        }
      );
    } else {
      this.snackBar.open(validate, 'Cerrar', { duration: 3000, panelClass: ["snackbar-error"] });
    }
  }

  validate(business: BusinessModel): string {
    let state = true
    if (!business.name) {
      return "Error en el campo del negocio";
    }
    else if (business.mail) {
      if (!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(business.mail)) {
        return "Correo no válido.";
      }
    }
    return "SUCCESS";
  }
}
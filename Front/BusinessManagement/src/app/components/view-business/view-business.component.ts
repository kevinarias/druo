import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';

import { BusinessService } from '../../services/business.service';
import { MatSort, MatTableDataSource} from '@angular/material';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-view-business',
  templateUrl: './view-business.component.html',
  styleUrls: ['./view-business.component.scss'],
  providers: [BusinessService]
})
export class ViewBusinessComponent implements OnInit{

  public businessList = [];
  displayedColumns: string[] = ['id', 'name', 'nit', 'mail'];
  messageEmpty: string = "Aún no hay negocios creados";
  displayTable: boolean = false;
  dataSource;


  @ViewChild(MatSort) sort: MatSort;

  constructor(private BusinessService: BusinessService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getBusiness();
  }
  
  getBusiness() {
    this.BusinessService.getBusiness().subscribe(
      res => {
        this.businessList = res;
        this.dataSource = new MatTableDataSource(this.businessList);        
        this.dataSource.sort = this.sort;
        if(this.businessList && this.businessList.length) this.displayTable = true;
      }, error => {
        let err = "Error desconocido."
        this.snackBar.open(err, 'Cerrar', { duration: 100000, panelClass: ["snackbar-error"] });
      }
    );
  }
}
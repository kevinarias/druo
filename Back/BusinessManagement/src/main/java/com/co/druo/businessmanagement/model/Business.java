package com.co.druo.businessmanagement.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_BM_BUSINESS")
public class Business {

	@Id
	@SequenceGenerator(name = "SEQ_GEN", sequenceName = "BUSINESS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GEN")
	long id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "NIT")
	private String nit;

	@Column(name = "MAIL")
	private String mail;

	public Business() {
		super();
	}

	public Business(String name, String nit, String email) {
		super();
		this.name = name;
		this.nit = nit;
		this.mail = email;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

}

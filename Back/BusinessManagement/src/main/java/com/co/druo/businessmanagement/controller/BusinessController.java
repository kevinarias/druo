package com.co.druo.businessmanagement.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.co.druo.businessmanagement.model.Business;
import com.co.druo.businessmanagement.repository.BusinessRepository;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RestController
public class BusinessController {

	@Autowired
	protected BusinessRepository businessRepository;

	@RequestMapping(value = "/business", method = RequestMethod.GET)
	@Transactional(timeout = 5)
	public ResponseEntity<List<Business>> getBusiness() {
		return ResponseEntity.ok(this.businessRepository.findAll());
	}

	@RequestMapping(value = "/business/create", method = RequestMethod.POST)
	@Transactional(timeout = 5)
	public ResponseEntity<Business> saveBusiness(@RequestBody Business business)
			throws JsonParseException, JsonMappingException, IOException {
		
		if (business.getName() == null || business.getName().isEmpty())
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		
		List<Business> list = this.businessRepository.findAll();
		boolean flag = true;
		for (Business b : list) { 
			if (b.getName().equals(business.getName()))	flag = false;
		}		
		
		if (flag) return ResponseEntity.ok(this.businessRepository.save(business));
		return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
	}
}

package com.co.druo.businessmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.co.druo.businessmanagement.model.Business;

@Repository
public interface BusinessRepository extends JpaRepository<Business, Integer>{

	
}

package com.co.druo.businessmanagement;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.co.druo.businessmanagement.controller.BusinessController;
import com.co.druo.businessmanagement.model.Business;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import org.junit.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BusinessManagementApplicationTests {

	private Business business;

	private static final String NAME = "Kevin Arias Inc";
	private static final String NIT = "12345678";
	private static final String MAIL = "ingkevinarias@gmail.com";

	private final BusinessController businessController = new BusinessController();

	@Before
	public void prepare() {
		business = new Business();
		business.setName(NAME);
		business.setMail(MAIL);
		business.setNit(NIT);
	}

	@Test
	public void emptyName() throws JsonParseException, JsonMappingException, IOException {
		business.setName("");
		Assert.assertNotNull(business);
		Assert.assertNotNull(business.getName());
		Assert.assertEquals("", business.getName());
		Assert.assertEquals(new ResponseEntity<>(HttpStatus.BAD_REQUEST), businessController.saveBusiness(business));
	}

}
